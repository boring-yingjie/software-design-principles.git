package com.jie.principles.relyonreverse;
/**
 * @description:测试类
 * @author: jie
 * @time: 2022/1/28 15:31
 */
public class Test {
    public static void main(String[] args) {
        Teacher teacher =new Teacher();
        teacher.receive(new WinXin("微信发来的消息"));
        teacher.receive(new QQ("qq发来的消息"));
        teacher.receive(new DingDing("钉钉发来的消息"));
    }
}