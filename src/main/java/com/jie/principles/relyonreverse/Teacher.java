package com.jie.principles.relyonreverse;
/**
 * @description:老师
 * @author: jie
 * @time: 2022/1/28 15:30
 */
public class Teacher {
    public void receive(App app){
        System.out.println(app.getInfo());
    }
}