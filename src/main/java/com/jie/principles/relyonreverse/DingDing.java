package com.jie.principles.relyonreverse;
/**
 * @description:钉钉
 * @author: jie
 * @time: 2022/1/28 15:36
 */
class DingDing implements App {
    private String info;

    public DingDing(String info) {
        this.info = info;
    }

    @Override
    public String getInfo() {
        return info;
    }
}