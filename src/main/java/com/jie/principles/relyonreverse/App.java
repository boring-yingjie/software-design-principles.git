package com.jie.principles.relyonreverse;
/**
 * @description:接口App
 * @author: jie
 * @time: 2022/1/28 15:47
 */
interface App{
    String getInfo();
}