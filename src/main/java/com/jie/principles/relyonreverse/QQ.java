package com.jie.principles.relyonreverse;
/**
 * @description:QQ
 * @author: jie
 * @time: 2022/1/28 15:31
 */
class QQ implements App{
    private String info;

    public QQ(String info) {
        this.info = info;
    }

    @Override
    public String getInfo() {
        return info;
    }
}