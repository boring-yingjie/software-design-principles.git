package com.jie.principles.relyonreverse;
/**
 * @description:微信
 * @author: jie
 * @time: 2022/1/28 15:31
 */
class WinXin implements App{
    private String info;

    public WinXin(String info) {
        this.info = info;
    }

    @Override
    public String getInfo() {
        return info;
    }
}