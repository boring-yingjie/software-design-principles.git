package com.jie.principles.Dimit;

/**
 * @description:客户端
 * @author: jie
 * @time: 2022/1/28 18:47
 */
public class Client {

    public static void main(String[] args) {
        //1、创建经纪人类
        Agent agent = new Agent();
        //2、创建明星对象
        Star star = new Star("张三");
        agent.setStar(star);
        //创建粉丝对象
        Fans fans = new Fans("李四");
        agent.setFans(fans);
        //创建媒体公司对象
        Company company = new Company("英杰媒体公司");
        agent.setCompany(company);

        //和粉丝
        agent.meeting();
        //和媒体公司
        agent.business();
    }

}
