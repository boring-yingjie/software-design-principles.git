package com.jie.principles.Dimit;
/**
 * @description:经纪人
 * @author: jie
 * @time: 2022/1/28 18:36
 */
public class Agent implements Meeting,Business{
    /**
     * @description:明星
     * @author: jie
     * @time: 2022/1/28 18:37
     */
    private Star star;
    /**
     * @description:粉丝
     * @author: jie
     * @time: 2022/1/28 18:37
     */
    private Fans fans;
    /**
     * @description:媒体公司
     * @author: jie
     * @time: 2022/1/28 18:37
     */
    private Company company;

    public void setStar(Star star) {
        this.star = star;
    }

    public void setFans(Fans fans) {
        this.fans = fans;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    /**
     * @description:和粉丝见面的方法
     * @author: jie
     * @time: 2022/1/28 18:38
     */
    @Override
    public void meeting() {
        System.out.println(fans.getName() + "与明星" + star.getName() + "见面了。");
    }
    /**
     * @description:和媒体公司洽谈的方法
     * @author: jie
     * @time: 2022/1/28 18:38
     */
    @Override
    public void business() {
        System.out.println(company.getName() + "与明星" + star.getName() + "洽淡业务。");
    }
}