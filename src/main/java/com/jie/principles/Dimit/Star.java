package com.jie.principles.Dimit;
/**
 * @description:明星
 * @author: jie
 * @time: 2022/1/28 18:35
 */
public class Star {
    private String name;

    public Star(String name) {
        this.name=name;
    }

    public String getName() {
        return name;
    }
}