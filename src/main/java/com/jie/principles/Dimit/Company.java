package com.jie.principles.Dimit;
/**
 * @description:媒体公司
 * @author: jie
 * @time: 2022/1/28 18:36
 */
public class Company {
    private String name;

    public Company(String name) {
        this.name=name;
    }

    public String getName() {
        return name;
    }
}