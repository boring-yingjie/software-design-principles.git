package com.jie.principles.Richter;
/**
 * @description:最基础的算数型的计算器
 * @author: jie
 * @time: 2022/1/28 13:53
 */
class BaseCountCal extends Cal {

    @Override
    protected double minus(double x, double y) {
        return x - y;
    }
}