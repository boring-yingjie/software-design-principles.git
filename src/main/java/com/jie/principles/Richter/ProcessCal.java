package com.jie.principles.Richter;
/**
 * @description:完成程序计算器的减法
 * @author: jie
 * @time: 2022/1/28 14:05
 */
class ProcessCal extends Cal{

    /**
     * @description:聚合Cal抽象类，不变的引用它，变化的不引用
     * @author: jie
     * @time: 2022/1/28 14:33
     */
    private Cal cal;

    public ProcessCal(Cal cal) {
        this.cal = cal;
    }

    @Override
    protected double minus(double x, double y) {
        return cal.minus(x, y) ;
    }

}