package com.jie.principles.Richter;
/**
 * @description:所有计算机的父类
 * @author: jie
 * @time: 2022/1/28 13:49
 */
public abstract class Cal {
    protected abstract double minus(double x, double y);
}