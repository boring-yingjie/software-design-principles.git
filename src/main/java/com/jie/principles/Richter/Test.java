package com.jie.principles.Richter;

/**
 * @description:测试类
 * @author: jie
 * @time: 2022/1/28 13:55
 */
public class Test {
    public static void main(String[] args) {
        Cal cal = new BaseCountCal();
        //完成6与3的减法（基本算数计算器）
        System.out.println(cal.minus(6, 3));

        //完成6与3的减法（程序计算器）
        ProcessCal processCal = new ProcessCal(cal);
        System.out.println(processCal.minus(6, 3));
    }
}