package com.jie.principles.lockage;

/**
 * @description:测试类
 * @author: jie
 * @time: 2022/1/28 12:12
 */
public class Test {

    public static void main(String[] args) {
        //默认皮肤
        defaultSkin();
        //战斗学院皮肤
        battleAcademy();
        //神王
        gods();

    }

    /**
     * @description:默认皮肤
     * @author: jie
     * @time: 2022/1/28 12:27
     */
    public static void defaultSkin() {
        //1、创建盖伦对象
        Garen garen = new Garen();
        //2、创建默认皮肤对象
        DefaultSkin defaultSkin = new DefaultSkin();
        //3、将皮肤设置给盖伦
        garen.setSkin(defaultSkin);
        //4、显示皮肤
        garen.display();
    }

    /**
     * @description:战斗学院
     * @author: jie
     * @time: 2022/1/28 12:31
     */
    public static void battleAcademy() {
        //1、创建盖伦对象
        Garen garen = new Garen();
        //2、创建战斗皮肤对象
        BattleAcademy battleAcademy = new BattleAcademy();
        //3、将皮肤设置给盖伦
        garen.setSkin(battleAcademy);
        //4、显示皮肤
        garen.display();
    }
    /**
     * @description:神王
     * @author: jie
     * @time: 2022/1/28 12:49
     */
    public static void gods() {
        //1、创建盖伦对象
        Garen garen = new Garen();
        //2、创建神王皮肤对象
        Gods gods = new Gods();
        //3、将皮肤设置给盖伦
        garen.setSkin(gods);
        //4、显示皮肤
        garen.display();
    }
}
