package com.jie.principles.lockage;
/**
 * @description:盖伦
 * @author: jie 
 * @time: 2022/1/28 12:07
 */
public class Garen {

    /**
     * @description:皮肤
     * @author: jie
     * @time: 2022/1/28 12:11
     */
    private Skin skin;

    public Skin getSkin() {
        return skin;
    }

    public void setSkin(Skin skin) {
        this.skin = skin;
    }


    public void display(){
        skin.display();
    }
}
