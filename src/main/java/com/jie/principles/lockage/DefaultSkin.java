package com.jie.principles.lockage;
/**
 * @description:默认皮肤德玛西亚之力
 * @author: jie
 * @time: 2022/1/28 12:02
 */
public class DefaultSkin extends Skin{

    @Override
    public void display() {
        System.out.println("德玛西亚之力：盖伦");
    }
}
