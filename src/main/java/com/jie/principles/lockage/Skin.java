package com.jie.principles.lockage;
/**
 * @description:抽象皮肤类
 * @author: jie 
 * @time: 2022/1/28 11:00
 */
public abstract class Skin {

    /**
     * @description:显示皮肤
     * @author: jie
     * @time: 2022/1/28 12:18
     */
    public abstract void display();

}
