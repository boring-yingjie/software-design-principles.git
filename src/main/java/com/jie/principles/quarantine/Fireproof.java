package com.jie.principles.quarantine;
/**
 * @description:防火接口
 * @author: jie
 * @time: 2022/1/28 17:57
 */
public interface Fireproof {
    void fireproof();
}