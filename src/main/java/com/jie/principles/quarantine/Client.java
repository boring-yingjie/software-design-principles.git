package com.jie.principles.quarantine;

import com.jie.principles.lockage.BattleAcademy;

/**
 * @description:客户端
 * @author: jie
 * @time: 2022/1/28 16:41
 */
public class Client {
    public static void main(String[] args) {
        //A品牌安全门
        ASafetyGate aSafetyGate = new ASafetyGate();
        aSafetyGate.antiTheft();
        aSafetyGate.fireproof();
        aSafetyGate.waterproof();
        System.out.println("_____________________________________________________");
        //B品牌安全门
        BSafetyGate bSafetyGate = new BSafetyGate();
        bSafetyGate.antiTheft();
    }
}
