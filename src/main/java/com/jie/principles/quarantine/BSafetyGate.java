package com.jie.principles.quarantine;
/**
 * @description:A品牌安全门
 * @author: jie
 * @time: 2022/1/28 16:32
 */
public class BSafetyGate implements AntiTheft{
    @Override
    public void antiTheft() {
        System.out.println("防盗");
    }
}
