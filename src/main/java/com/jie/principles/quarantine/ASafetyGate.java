package com.jie.principles.quarantine;
/**
 * @description:A品牌安全门
 * @author: jie
 * @time: 2022/1/28 16:32
 */
public class ASafetyGate implements AntiTheft,Fireproof,Waterproof{
    @Override
    public void antiTheft() {
        System.out.println("防盗");
    }

    @Override
    public void fireproof() {
        System.out.println("防火");
    }


    @Override
    public void waterproof() {
        System.out.println("防水");
    }
}
