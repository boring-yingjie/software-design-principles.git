package com.jie.principles.quarantine;
/**
 * @description:防盗接口
 * @author: jie
 * @time: 2022/1/28 17:57
 */
public interface AntiTheft {
    void antiTheft();
}