package com.jie.principles.quarantine;
/**
 * @description:防水接口
 * @author: jie
 * @time: 2022/1/28 17:56
 */
public interface Waterproof {
    void waterproof();
}